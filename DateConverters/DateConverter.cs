﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace DummyDatabase.Desktop.DateConverters
{
    public class DateConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value is DateTime)
            {
                return ((DateTime)value).ToString("dd.MM.yyyy");
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DateTime resultDateTime;
            if (DateTime.TryParseExact(value as string, "dd.MM.yyyy",
                                       CultureInfo.CurrentCulture, DateTimeStyles.None, out resultDateTime))
            {
                return resultDateTime;
            }
            return DependencyProperty.UnsetValue;
        }
    }
}
