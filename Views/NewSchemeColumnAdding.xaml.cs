﻿using DummyDatabase.Desktop.ViewModels;
using System.Windows;

namespace DummyDatabase.Desktop
{
    public partial class NewSchemeColumnAdding : Window
    {
        public NewSchemeColumnAdding(MainWindow mainWindow)
        {
            InitializeComponent();

            DataContext = new AddNewColumnViewModel(this, mainWindow);
        }
    }
}
