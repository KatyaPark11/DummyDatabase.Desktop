﻿using System.Windows.Forms;

namespace DummyDatabase.Desktop.View
{
    internal class FolderDialog
    {
        public static string GetThePath()
        {
            string path = null;
            using (var dialog = new FolderBrowserDialog())
                if (dialog.ShowDialog() == DialogResult.OK)
                    path = dialog.SelectedPath;
            return path;
        }
    }
}
