﻿using DummyDatabase.Core;
using DummyDatabase.Desktop.ViewModels;
using System.Collections.Generic;
using System.IO;
using System.Windows;

namespace DummyDatabase.Desktop
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {
        public static DirectoryInfo MainDirectory { get; set; }
        public static Table? CurrentTable { get; set; }
        public static bool IsTableChanged { get; set; }

        public MainWindow()
        {
            InitializeComponent();

            DataContext = new MainViewModel(this);
        }
    }
}