﻿using System.Windows;
using System.Windows.Media;

namespace DummyDatabase.Desktop.ViewModels
{
    public static class DependencyObjectExtensions
    {
        public static T GetParentOfType<T>(this DependencyObject child) where T : DependencyObject
        {
            DependencyObject parent = VisualTreeHelper.GetParent(child);

            while (parent != null && !(parent is T))
            {
                parent = VisualTreeHelper.GetParent(parent);
            }

            return parent as T;
        }
    }
}
