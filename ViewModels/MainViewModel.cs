﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;

namespace DummyDatabase.Desktop.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public ICommand OpenDatabase { get; private set; }
        public ICommand OpenSelectedFile { get; private set; }
        public ICommand CreateDatabase { get; private set; }
        public ICommand CreateFolder { get; private set; }
        public ICommand CreateFile { get; private set; }
        public ICommand DeleteFileSysElem { get; private set; }
        public ICommand RenameFileSysElem { get; private set; }
        public ICommand UpdateSelectedFile { get; private set; }
        public ICommand ExpandTreeViewItem { get; private set; }
        public ICommand SaveSelectedItemCommand { get; private set; }
        public ICommand DeselectTreeViewItem { get; private set; }
        public ICommand OpenToolTip { get; private set; }
        public ICommand RenameColumn { get; private set; }
        public ICommand AddNewColumn { get; private set; }
        public ICommand RemoveRow { get; private set; }
        public static ICommand RemoveColumn { get; private set; }
        public ICommand SaveData { get; private set; }
        public ICommand DeselectDataGridRow { get; private set; }
        public ICommand BeginCellEdit { get; private set; }
        public ICommand EndCellEdit { get; private set; }

        public MainViewModel(MainWindow window)
        {
            TreeViewViewModel treeViewViewModel = new TreeViewViewModel(window);
            DataGridViewModel dataGridViewModel = new DataGridViewModel(window);

            OpenDatabase = new RelayCommand(treeViewViewModel.ExecuteDatabaseOpening);
            OpenSelectedFile = new RelayCommand(treeViewViewModel.ExecuteSelectedFileOpening);
            CreateDatabase = new RelayCommand(treeViewViewModel.ExecuteDatabaseCreating);
            CreateFolder = new RelayCommand(treeViewViewModel.ExecuteFolderCreating);
            CreateFile = new RelayCommand(treeViewViewModel.ExecuteFileCreating);
            DeleteFileSysElem = new RelayCommand(treeViewViewModel.ExecuteFileSysElemDeletion);
            RenameFileSysElem = new RelayCommand(treeViewViewModel.ExecuteFileSysElemRenaming);
            UpdateSelectedFile = new RelayCommand(treeViewViewModel.ExecuteSelectedFileUpdate);
            ExpandTreeViewItem = new RelayCommand(treeViewViewModel.ExecuteTreeViewItemExpansion);
            DeselectTreeViewItem = new RelayCommand(treeViewViewModel.ExecuteTreeViewItemDeselecting);
            OpenToolTip = new RelayCommand(dataGridViewModel.ExecuteToolTipOpening);
            RenameColumn = new RelayCommand(dataGridViewModel.ExecuteColumnRenaming);
            AddNewColumn = new RelayCommand(dataGridViewModel.ExecuteNewColumnAddition);
            RemoveRow = new RelayCommand(dataGridViewModel.ExecuteRowRemoval);
            RemoveColumn = new RelayCommand(dataGridViewModel.ExecuteColumnRemoval);
            SaveData = new RelayCommand(dataGridViewModel.ExecuteDataSaving);
            DeselectDataGridRow = new RelayCommand(dataGridViewModel.ExecuteDataGridRowDeselecting);
            BeginCellEdit = new RelayCommand(dataGridViewModel.ExecuteCellEditBeginning);
            EndCellEdit = new RelayCommand(dataGridViewModel.ExecuteCellEditEnding);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
        protected void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
