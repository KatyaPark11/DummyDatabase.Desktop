﻿using DummyDatabase.Console;
using DummyDatabase.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace DummyDatabase.Desktop.ViewModels
{
    class AddNewColumnViewModel : INotifyPropertyChanged
    {
        private readonly NewSchemeColumnAdding addingWindow;
        private TextBox name { get; set; }
        private ComboBox type { get; set; }
        private CheckBox isForeignKey { get; set; }
        private ComboBox csvFiles { get; set; }
        private ComboBox columnNames { get; set; }
        private TextBox defaultValue { get; set; }
        private CheckBox defaultValueCheckBox { get; set; }
        private TreeView treeView { get; set; }

        public ICommand AddThisColumn { get; set; }
        public ICommand CloseThisForm { get; set; }
        public ICommand ChangeTextBoxBehaviour { get; set; }
        public ICommand ChangeComboBoxBehaviour { get; set; }
        public ICommand ChangeComboBoxAccess { get; set; }
        public ICommand DeleteComboBoxesSelectionItems { get; set; }

        public AddNewColumnViewModel(NewSchemeColumnAdding addingWindow, MainWindow mainWindow)
        {
            this.addingWindow = addingWindow;
            treeView = mainWindow.TreeView;
            name = addingWindow.Name;
            type = addingWindow.Type;
            isForeignKey = addingWindow.ForeignKeyCheckBox;
            csvFiles = addingWindow.CsvFiles;
            columnNames = addingWindow.ColumnNames;
            GetCsvFilesForComboBox();
            defaultValue = addingWindow.DefaultValue;
            defaultValueCheckBox = addingWindow.DefaultValueCheckBox;

            AddThisColumn = new RelayCommand(ExecuteThisColumnCheckingAndAdding);
            CloseThisForm = new RelayCommand(ExecuteThisFormClosing);
            ChangeTextBoxBehaviour = new RelayCommand(ExecuteTextBoxBehaviourChanging);
            ChangeComboBoxBehaviour = new RelayCommand(ExecuteComboBoxBehaviourChanging);
            ChangeComboBoxAccess = new RelayCommand(ExecuteComboBoxChanging);
            DeleteComboBoxesSelectionItems = new RelayCommand(ExecuteComboBoxesSelectedItemsDeletion);
        }

        private void ExecuteThisColumnCheckingAndAdding(object param)
        {
            List<string> missingData = new List<string>();
            if (string.IsNullOrEmpty(name.Text))
                missingData.Add(name.Name.ToString());
            if (string.IsNullOrEmpty(type.Text))
                missingData.Add(type.Name.ToString());
            if ((bool)isForeignKey.IsChecked && (csvFiles.SelectedItem == null || columnNames.SelectedItem == null))
                missingData.Add(csvFiles.Name.ToString());
            if (missingData.Count > 0)
            {
                Error error = new Error((int)ErrorsDisplayer.ErrorIndices.DataAbsenceIndex, missingData);
                MessageBox.Show(error.Message, error.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else if ((bool)defaultValueCheckBox.IsChecked)
            {
                if (!DataExamination.IsRightType(defaultValue.Text, type.Text))
                {
                    Error error = new Error((int)ErrorsDisplayer.ErrorIndices.DefaultValueTypeIndex, defaultValue.Text, type.Text);
                    MessageBox.Show(error.Message, error.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                Table primaryTable = TableInitializer.Tables[TableInitializer.FromFileNameToTableListIndex[csvFiles.Text]];
                if ((bool)isForeignKey.IsChecked && !primaryTable.PrimaryElemsList.Contains(defaultValue.Text))
                {
                    Error error = new Error((int)ErrorsDisplayer.ErrorIndices.PrimaryKeyColValueExistenceIndex, 
                                  primaryTable.Name, primaryTable.TableFileInfo.FullName, columnNames.Text, defaultValue.Text);
                    MessageBox.Show(error.Message, error.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            else if (MainWindow.CurrentTable.DataTable.Columns.Contains(name.Text))
            {
                Error sameColumnNameError = new Error((int)ErrorsDisplayer.ErrorIndices.SameColumnNameIndex);
                MessageBox.Show(sameColumnNameError.Message, sameColumnNameError.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            
            AddColumn();
            DataGridViewModel.UpdateTheDataGrid();
            DataGridViewModel.AddColumnNameToLists(MainWindow.CurrentTable.Scheme.Columns.Count - 1);
            MainWindow.IsTableChanged = true;
            addingWindow.Close();
        }

        private void ExecuteThisFormClosing(object param)
        {
            addingWindow.Close();
        }

        private void ExecuteTextBoxBehaviourChanging(object param)
        {
            bool isChecked = Convert.ToBoolean(param);
            defaultValue.IsReadOnly = !isChecked;
        }

        private void ExecuteComboBoxBehaviourChanging(object param)
        {
            if (type.SelectedItem != null)
            {
                bool isChecked = Convert.ToBoolean(param);
                csvFiles.IsEnabled = isChecked;
                if (csvFiles.SelectedItem != null)
                    columnNames.IsEnabled = isChecked;
            }
            else
            {
                isForeignKey.IsChecked = false;
            }
        }

        private void ExecuteComboBoxChanging(object param)
        {
            ComboBoxItem selectedItem = csvFiles.SelectedItem as ComboBoxItem;
            if (selectedItem != null)
            {
                FileInfo tablePath = selectedItem.Tag as FileInfo;
                Table table = TableInitializer.Tables[TableInitializer.FromFileNameToTableListIndex[tablePath.Name]];
                Error? tableInitializeError = null;
                if (!table.IsInitialized)
                    tableInitializeError = TableInitializer.TryToInitializeTheTable(table);

                if (tableInitializeError == null)
                {
                    if (csvFiles.SelectedItem != null)
                    {
                        if (table.DataTable.Rows.Count == 0)
                        {
                            Error emptySelectedTableError = new Error((int)ErrorsDisplayer.ErrorIndices.EmptySelectedTableIndex, table.Name);
                            MessageBox.Show(emptySelectedTableError.Message, emptySelectedTableError.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                            return;
                        }
                        GetColumnNamesForComboBox(table);
                    }
                    else
                    {
                        columnNames.IsEnabled = false;
                    }
                }
                else
                {
                    MessageBox.Show(tableInitializeError.Message, tableInitializeError.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                    selectedItem.IsSelected = false;
                }
            }
        }

        private void ExecuteComboBoxesSelectedItemsDeletion(object param)
        {
            csvFiles.Items.Clear();
            columnNames.Items.Clear();
            csvFiles.IsEnabled = false;
            columnNames.IsEnabled = false;
            isForeignKey.IsChecked = false;
            GetCsvFilesForComboBox();
        }

        private void GetColumnNamesForComboBox(Table table)
        {
            if (table.PrimaryColumnIndex != -1 && table.Scheme.Columns[table.PrimaryColumnIndex].Type == type.Text)
            {
                ComboBoxItem comboBoxItem = new ComboBoxItem();
                comboBoxItem.Content = table.Scheme.Columns[table.PrimaryColumnIndex].Name;
                columnNames.Items.Add(comboBoxItem);
                columnNames.IsEnabled = true;
            }
            else
            {
                foreach (SchemeColumn column in table.Scheme.Columns)
                    if (column.Type == type.Text)
                    {
                        ComboBoxItem comboBoxItem = new ComboBoxItem();
                        comboBoxItem.Content = column.Name;
                        columnNames.Items.Add(comboBoxItem);
                    }
                columnNames.IsEnabled = true;
            }
        }

        private void GetCsvFilesForComboBox()
        {
            foreach (object item in treeView.Items)
                if (item is TreeViewItem rootItem)
                     AddCsvFileNames(rootItem);
        }

        private void AddCsvFileNames(TreeViewItem treeViewItem)
        {
            if (treeViewItem.Tag is FileInfo file && file.Name != MainWindow.CurrentTable.Name)
            {
                ComboBoxItem comboBoxItem = new ComboBoxItem();
                comboBoxItem.Content = treeViewItem.Header;
                comboBoxItem.Tag = treeViewItem.Tag;
                csvFiles.Items.Add(comboBoxItem);
            }

            foreach (object item in treeViewItem.Items)
                if (item is TreeViewItem subItem)
                    AddCsvFileNames(subItem);
        }

        private void AddColumn()
        {
            SchemeColumn schemeColumn = new SchemeColumn();
            schemeColumn.Name = name.Text;
            string typeName = SchemeColumn.TypeAbbreviations[this.type.Text];
            Type type = Type.GetType(typeName);
            schemeColumn.Type = this.type.Text;

            if ((bool)isForeignKey.IsChecked)
            {
                schemeColumn.IsForeignKey = true;
                schemeColumn.PrimaryTableLink = new Link();
                ComboBoxItem comboBoxItem = csvFiles.SelectedItem as ComboBoxItem;
                schemeColumn.PrimaryTableLink.FilePath = comboBoxItem.Tag.ToString();
                schemeColumn.PrimaryTableLink.FileName = csvFiles.Text;
                schemeColumn.PrimaryTableLink.ColumnName = columnNames.Text;
                schemeColumn.PrimaryTable = TableInitializer.Tables[TableInitializer.FromFileNameToTableListIndex[csvFiles.Text]];
                schemeColumn.ForeignColumnIndex = MainWindow.CurrentTable.Scheme.Columns.Count;
                if (schemeColumn.PrimaryTable.PrimaryColumnIndex == -1)
                {
                    schemeColumn.PrimaryTable.PrimaryColumnIndex = columnNames.SelectedIndex;
                    schemeColumn.PrimaryTable.Scheme.Columns[columnNames.SelectedIndex].IsPrimaryKey = true;
                }
                Link foreignTableLink = new Link();
                foreignTableLink.FilePath = MainWindow.CurrentTable.TableFileInfo.FullName;
                foreignTableLink.FileName = MainWindow.CurrentTable.Name;
                foreignTableLink.ColumnName = schemeColumn.Name;
                SchemeColumn primaryKeyColumn = schemeColumn.PrimaryTable.Scheme.Columns[columnNames.SelectedIndex];
                primaryKeyColumn.ForeignTablesLinks.Add(foreignTableLink);
                primaryKeyColumn.ForeignTables.Add(MainWindow.CurrentTable);
            }

            MainWindow.CurrentTable.Scheme.Columns.Add(schemeColumn);
            MainWindow.CurrentTable.DataTable.Columns.Add(schemeColumn.Name, type);

            if ((bool)defaultValueCheckBox.IsChecked)
            {
                foreach (DataRow row in MainWindow.CurrentTable.DataTable.Rows)
                    row.SetField(schemeColumn.Name, defaultValue.Text);
            }
            else
            {
                if ((bool)isForeignKey.IsChecked)
                    foreach (DataRow row in MainWindow.CurrentTable.DataTable.Rows)
                        row.SetField(schemeColumn.Name, schemeColumn.PrimaryTable.PrimaryElemsList[0]);
                else
                    foreach (DataRow row in MainWindow.CurrentTable.DataTable.Rows)
                        row.SetField(schemeColumn.Name, SchemeColumn.DefaultValues[typeName]);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        protected void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
