﻿using DummyDatabase.Console;
using DummyDatabase.Core;
using DummyDatabase.Desktop.View;
using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace DummyDatabase.Desktop.ViewModels
{
    public class TreeViewViewModel
    {
        private static TreeView treeView { get; set; }
        private static DataGrid dataGrid { get; set; }
        private static ComboBox newColumnNameComboBox { get; set; }
        private static MenuItem removeColumn { get; set; }
        private static TextBlock mainDirectoryName { get; set; }

        public TreeViewViewModel(MainWindow window)
        {
            treeView = window.TreeView;
            dataGrid = window.DataGrid;
            newColumnNameComboBox = window.NewColumnNameComboBox;
            removeColumn = window.RemoveColumn;
            mainDirectoryName = window.MainDirectoryName;
        }

        public void ExecuteDatabaseOpening(object param)
        {
            string path = FolderDialog.GetThePath();

            if (path != null && Directory.Exists(path))
            {
                if (MainWindow.CurrentTable != null)
                {
                    ClearTheDataTable();
                }
                CreateTheTreeView(path);
                AddTheObjects(MainWindow.MainDirectory, treeView);
                GetAllTables(MainWindow.MainDirectory);
                MainWindow.IsTableChanged = false;
            }
        }

        public static void GetAllTables(DirectoryInfo directory)
        {
            foreach (FileInfo file in directory.GetFiles("*.csv"))
            {
                FileInfo scheme = new FileInfo($"{file.FullName.Remove(file.FullName.Length - file.Extension.Length)}Scheme.json");
                TableInitializer.Tables.Add(new Table(file, scheme));
                TableInitializer.FromFileNameToTableListIndex.Add(file.Name, TableInitializer.FromFileNameToTableListIndex.Count);
            }

            foreach (DirectoryInfo subDir in directory.GetDirectories())
                if ((subDir.Attributes & FileAttributes.Hidden) == 0)
                    GetAllTables(subDir);
        }

        public void ExecuteSelectedFileOpening(object param)
        {
            TreeViewItem selectedItem = treeView.SelectedItem as TreeViewItem;
            if (selectedItem != null && selectedItem.Tag is FileInfo file)
            {
                if (!File.Exists(file.FullName))
                {
                    Error error = new Error((int)ErrorsDisplayer.ErrorIndices.ExtracurricularDataChangingIndex);
                    MessageBox.Show(error.Message, error.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    DataGridViewModel.DisplayCsvInDataGrid(selectedItem);
                }
            }
        }

        public void ExecuteDatabaseCreating(object param)
        {
            string path = FolderDialog.GetThePath();

            if (path != null && Directory.Exists(path))
            {
                if (Directory.EnumerateFileSystemEntries(path).Any())
                {
                    Error notEmptyFolderError = new Error((int)ErrorsDisplayer.ErrorIndices.NotEmptyFolderIndex);
                    MessageBox.Show(notEmptyFolderError.Message, notEmptyFolderError.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    if (MainWindow.CurrentTable != null)
                        ClearTheDataTable();
                    CreateTheTreeView(path);
                    MainWindow.IsTableChanged = false;
                }
            }
        }

        public void ExecuteFolderCreating(object param)
        {
            if (!string.IsNullOrEmpty(mainDirectoryName.Text))
            {
                object obj = TryGetFolderPath();
                if (obj is string directoryPath)
                {
                    string directoryName = Microsoft.VisualBasic.Interaction.InputBox("Введите имя папки", "Создание папки", "");
                    if (!string.IsNullOrEmpty(directoryName))
                    {
                        string subDirectoryPath = Path.Combine(directoryPath, directoryName);
                        Error? fileSysElemNameError = IsFileSysElemNameCorrect(directoryName, directoryPath);

                        if (fileSysElemNameError == null)
                        {
                            if (!Directory.Exists(subDirectoryPath))
                            {
                                Directory.CreateDirectory(subDirectoryPath);
                                DirectoryInfo newDirectory = new DirectoryInfo(subDirectoryPath);

                                TreeViewItem item = treeView.SelectedItem as TreeViewItem;
                                if (item == null)
                                    treeView.Items.Add(CreateTreeViewItem(newDirectory, newDirectory.Name));
                                else if (item.Tag is DirectoryInfo)
                                    item.Items.Add(CreateTreeViewItem(newDirectory, newDirectory.Name));
                            }
                            else
                            {
                                Error folderExistenceError = new Error((int)ErrorsDisplayer.ErrorIndices.FolderExistenceIndex);
                                MessageBox.Show(folderExistenceError.Message, folderExistenceError.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show(fileSysElemNameError.Message, fileSysElemNameError.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
                else
                {
                    Error selectedPlaceError = obj as Error;
                    MessageBox.Show(selectedPlaceError.Message, selectedPlaceError.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        public void ExecuteFileCreating(object param)
        {
            if (!string.IsNullOrEmpty(mainDirectoryName.Text))
            {
                object obj = TryGetFolderPath();
                if (obj is string directoryPath)
                {
                    string fileName = Microsoft.VisualBasic.Interaction.InputBox("Введите имя файла", "Создание файла", "");
                    if (!string.IsNullOrEmpty(fileName))
                    {
                        if (!fileName.EndsWith(".csv"))
                        {
                            fileName += ".csv";
                        }
                        string filePath = Path.Combine(directoryPath, fileName);
                        Error? fileSysElemNameError = IsFileSysElemNameCorrect(fileName, directoryPath);

                        if (fileSysElemNameError == null)
                        {
                            if (!TableInitializer.FromFileNameToTableListIndex.ContainsKey(fileName))
                            {
                                File.WriteAllText(filePath, string.Empty);
                                FileInfo file = new FileInfo(filePath);

                                Scheme scheme = new Scheme();
                                FileInfo schemeFileInfo = new FileInfo($"{file.FullName.Remove(file.FullName.Length - file.Extension.Length)}Scheme.json");
                                string newSchemeText = JsonConvert.SerializeObject(scheme, Formatting.Indented | Formatting.None);
                                File.WriteAllText(schemeFileInfo.FullName, newSchemeText);

                                TreeViewItem item = treeView.SelectedItem as TreeViewItem;
                                if (item == null)
                                    treeView.Items.Add(CreateTreeViewItem(file, file.Name));
                                else if (item.Tag is DirectoryInfo)
                                    item.Items.Add(CreateTreeViewItem(file, file.Name));
                                
                                TableInitializer.Tables.Add(new Table(file, schemeFileInfo));
                                TableInitializer.FromFileNameToTableListIndex.Add(file.Name, TableInitializer.FromFileNameToTableListIndex.Count);
                            }
                            else
                            {
                                Error fileNameError = new Error((int)ErrorsDisplayer.ErrorIndices.FileNameIndex);
                                MessageBox.Show(fileNameError.Message, fileNameError.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show(fileSysElemNameError.Message, fileSysElemNameError.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
                else
                {
                    Error selectedPlaceError = obj as Error;
                    MessageBox.Show(selectedPlaceError.Message, selectedPlaceError.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        public static object TryGetFolderPath()
        {
            TreeViewItem item = treeView.SelectedItem as TreeViewItem;
            if (item == null)
                return MainWindow.MainDirectory.FullName;
            else if (item.Tag is DirectoryInfo directoryInfo)
                return directoryInfo.FullName;
            else
                return new Error((int)ErrorsDisplayer.ErrorIndices.SelectedPlaceIndex);
        }

        public void ExecuteFileSysElemDeletion(object param)
        {
            TreeViewItem selectedItem = treeView.SelectedItem as TreeViewItem;
            if (selectedItem == null)
            {
                return;
            }
            else if (selectedItem.Tag is DirectoryInfo directoryInfo)
            {
                Directory.Delete(directoryInfo.FullName, true);
            }
            else if (selectedItem.Tag is FileInfo fileInfo)
            {
                Table table = TableInitializer.Tables[TableInitializer.FromFileNameToTableListIndex[fileInfo.Name]];
                if (table.IsInitialized)
                {
                    if (table.PrimaryColumnIndex != -1)
                    {
                        SchemeColumn primaryKeyColumn = table.Scheme.Columns[table.PrimaryColumnIndex];
                        if (primaryKeyColumn.ForeignTablesLinks.Count > 0)
                        {
                            Error linkExistenceError = new Error((int)ErrorsDisplayer.ErrorIndices.LinkExistenceIndex);
                            MessageBox.Show(linkExistenceError.Message, linkExistenceError.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                            return;
                        }
                    }
                    foreach (SchemeColumn column in table.Scheme.Columns)
                    {
                        if (column.IsForeignKey)
                            DataGridViewModel.RemoveForeignTableLink(column);
                    }
                }
                File.Delete(table.TableFileInfo.FullName);
                File.Delete(table.SchemeFileInfo.FullName);
                TableInitializer.Tables.Remove(table);
                TableInitializer.FromFileNameToTableListIndex.Remove(table.TableFileInfo.Name);
            }

            TreeViewItem parent = selectedItem.Parent as TreeViewItem;
            if (parent != null)
                parent.Items.Remove(selectedItem);
            else
                treeView.Items.Remove(selectedItem);
        }

        public void ExecuteFileSysElemRenaming(object param)
        {
            TreeViewItem selectedItem = treeView.SelectedItem as TreeViewItem;
            if (selectedItem.Tag is DirectoryInfo) 
            {
                string path = selectedItem.Tag.ToString();
                string directoryName = Microsoft.VisualBasic.Interaction.InputBox("Введите новое имя папки", "Переименование папки", "");
                if (!string.IsNullOrEmpty(directoryName)) 
                {
                    string directoryPath = Path.GetDirectoryName(path);
                    string newPath = Path.Combine(directoryPath, directoryName);
                    Error? fileSysElemNameError = IsFileSysElemNameCorrect(directoryName, newPath);

                    if (fileSysElemNameError == null) 
                    {
                        if (!Directory.Exists(newPath)) 
                        {
                            try 
                            {
                                Directory.Move(path, newPath);
                            }
                            catch 
                            {
                                return;
                            }

                            selectedItem.Header = directoryName;
                            DirectoryInfo directoryInfo = new DirectoryInfo(newPath);
                            selectedItem.Tag = directoryInfo;
                        }
                        else 
                        {
                            Error error = new Error((int)ErrorsDisplayer.ErrorIndices.FolderExistenceIndex);
                            MessageBox.Show(error.Message, error.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show(fileSysElemNameError.Message, fileSysElemNameError.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            else if (selectedItem.Tag is FileInfo fileInfo)
            {
                Table table = TableInitializer.Tables[TableInitializer.FromFileNameToTableListIndex[fileInfo.Name]];
                string fileName = Microsoft.VisualBasic.Interaction.InputBox("Введите новое имя файла", "Переименование файла", "");
                if (!string.IsNullOrEmpty(fileName)) 
                {
                    if (!fileName.EndsWith(".csv"))
                        fileName += ".csv";
                    
                    string directoryPath = table.TableFileInfo.Directory.FullName;
                    string newPath = Path.Combine(directoryPath, fileName);
                    FileInfo newFileInfo = new FileInfo(newPath);
                    FileInfo newSchemeFileInfo = new FileInfo($"{newPath.Remove(newFileInfo.FullName.Length - newFileInfo.Extension.Length)}Scheme.json");
                    Error? fileSysElemNameError = IsFileSysElemNameCorrect(fileName, newPath);

                    if (fileSysElemNameError == null) 
                    {
                        if (!TableInitializer.FromFileNameToTableListIndex.ContainsKey(fileName)) 
                        {
                            try 
                            {
                                File.Move(table.TableFileInfo.FullName, newPath);
                                File.Move(table.SchemeFileInfo.FullName, 
                                          newSchemeFileInfo.FullName);
                            }
                            catch 
                            {
                                return;
                            }

                            table.TableFileInfo = newFileInfo;
                            table.Name = table.TableFileInfo.Name;
                            table.SchemeFileInfo = newSchemeFileInfo;
                            TableInitializer.FromFileNameToTableListIndex.Add(newFileInfo.Name, TableInitializer.FromFileNameToTableListIndex[fileInfo.Name]);
                            TableInitializer.FromFileNameToTableListIndex.Remove(fileInfo.Name);
                            selectedItem.Header = fileName;
                            selectedItem.Tag = newFileInfo;
                        }
                        else 
                        {
                            Error error = new Error((int)ErrorsDisplayer.ErrorIndices.FileNameIndex);
                            MessageBox.Show(error.Message, error.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show(fileSysElemNameError.Message, fileSysElemNameError.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            else
            {
                return;
            }
        }

        public void ExecuteSelectedFileUpdate(object param)
        {
            TreeViewItem selectedItem = treeView.SelectedItem as TreeViewItem;
            if (selectedItem != null && selectedItem.Tag is FileInfo file)
            {
                if (!File.Exists(file.FullName))
                {
                    Error error = new Error((int)ErrorsDisplayer.ErrorIndices.ExtracurricularDataChangingIndex);
                    MessageBox.Show(error.Message, error.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                Table selectedTable = TableInitializer.Tables[TableInitializer.FromFileNameToTableListIndex[file.Name]];
                selectedTable.IsInitialized = false;
                Error? currentTableInitializeError = TableInitializer.TryToInitializeTheTable(selectedTable);

                if (currentTableInitializeError != null)
                {
                    MessageBox.Show(currentTableInitializeError.Message, currentTableInitializeError.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                    if (MainWindow.CurrentTable == selectedTable)
                        ClearTheDataTable();
                }
                else
                {
                    if (MainWindow.CurrentTable == selectedTable)
                        DataGridViewModel.UpdateTheDataGrid();
                }
            }
        }

        public void ExecuteTreeViewItemExpansion(object param)
        {
            RoutedEventArgs e = param as RoutedEventArgs;
            TreeViewItem item = e.Source as TreeViewItem;

            if (item.Tag is DirectoryInfo expandedDir) 
            {
                item.Items.Clear();
                if (!Directory.Exists(expandedDir.FullName)) 
                {
                    Error extracurricularDataChangingError = new Error((int)ErrorsDisplayer.ErrorIndices.ExtracurricularDataChangingIndex);
                    MessageBox.Show(extracurricularDataChangingError.Message, extracurricularDataChangingError.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                AddTheObjects(expandedDir, item);
            }
            else if (item.Tag is FileInfo file) 
            {
                if (!File.Exists(file.FullName)) 
                {
                    Error extracurricularDataChangingError = new Error((int)ErrorsDisplayer.ErrorIndices.ExtracurricularDataChangingIndex);
                    MessageBox.Show(extracurricularDataChangingError.Message, extracurricularDataChangingError.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                DataGridViewModel.DisplayCsvInDataGrid(item);
            }
        }

        public void ExecuteTreeViewItemDeselecting(object param)
        {
            if (treeView.SelectedItem != null)
            {
                TreeViewItem selectedItem = treeView.SelectedItem as TreeViewItem;
                selectedItem.IsSelected = false;
            }
        }

        public static Error? IsFileSysElemNameCorrect(string name, string path)
        {
            const int maxPathLength = 260;
            const int maxFileNameLength = 255;
            name = name.Trim();
            if (path.Length > maxPathLength || name.Length > maxFileNameLength) 
            {
                Error error = new Error((int)ErrorsDisplayer.ErrorIndices.FileSysElemNameLenIndex);
                MessageBox.Show(error.Message, error.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                return error;
            }

            char[] invalidChars = Path.GetInvalidFileNameChars();
            int index = name.IndexOfAny(invalidChars);
            if (index != -1) 
            {
                Error error = new Error((int)ErrorsDisplayer.ErrorIndices.ProhibitedCharsIndex);
                MessageBox.Show(error.Message, error.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                return error;
            }

            return null;
        }

        public static void CreateTheTreeView(string path)
        {
            TableInitializer.Tables.Clear();
            TableInitializer.FromFileNameToTableListIndex.Clear();
            treeView.Items.Clear();
            MainWindow.MainDirectory = new DirectoryInfo(path);
            mainDirectoryName.Text = MainWindow.MainDirectory.Name;
        }

        public static void ClearTheDataTable()
        {
            removeColumn.Items.Clear();
            newColumnNameComboBox.Items.Clear();
            MainWindow.CurrentTable = null;
            dataGrid.ItemsSource = null;
            dataGrid.Columns.Clear();
        }

        public static void AddTheObjects(DirectoryInfo expandedDir, TreeViewItem item)
        {
            foreach (DirectoryInfo subDir in expandedDir.GetDirectories())
                if ((subDir.Attributes & FileAttributes.Hidden) == 0)
                    item.Items.Add(CreateTreeViewItem(subDir, subDir.Name));

            foreach (FileInfo file in expandedDir.GetFiles("*.csv"))
                item.Items.Add(CreateTreeViewItem(file, file.Name));
        }

        private static void AddTheObjects(DirectoryInfo expandedDir, TreeView item)
        {
            foreach (DirectoryInfo subDir in expandedDir.GetDirectories())
                if ((subDir.Attributes & FileAttributes.Hidden) == 0)
                    item.Items.Add(CreateTreeViewItem(subDir, subDir.Name));

            foreach (FileInfo file in expandedDir.GetFiles("*.csv")) 
                item.Items.Add(CreateTreeViewItem(file, file.Name));
        }

        private static TreeViewItem CreateTreeViewItem(object obj, string name)
        {
            TreeViewItem item = new TreeViewItem();
            item.Header = name;
            item.Tag = obj;
            var converter = new BrushConverter();
            item.Foreground = (Brush)converter.ConvertFromString("#FFEBFFAB");
            if (obj is DirectoryInfo)
                item.Items.Add("Нужная пыпа");
            return item;
        }
    }
}
