﻿using System;
using System.Windows.Input;

namespace DummyDatabase.Desktop.ViewModels
{
    class RelayCommand : ICommand
    {
        private Action<object> execute;
        private Func<object, bool> canExecute;
        public RelayCommand(Action<object> action) => execute = action;
        public event EventHandler? CanExecuteChanged;

        public bool CanExecute(object? parameter)
        {
            return canExecute == null || canExecute(parameter);
        }

        public void Execute(object? parameter) => execute(parameter);
    }
}