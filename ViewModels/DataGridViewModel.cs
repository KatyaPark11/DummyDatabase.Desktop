﻿using DummyDatabase.Console;
using DummyDatabase.Core;
using DummyDatabase.Desktop.DateConverters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace DummyDatabase.Desktop.ViewModels
{
    public class DataGridViewModel
    {
        private readonly MainWindow window;
        private static DataGrid dataGrid { get; set; }
        private static ComboBox newColumnNameComboBox { get; set; }
        private static TextBox newColumnName { get; set; }
        private static MenuItem removeColumn { get; set; }

        public DataGridViewModel(MainWindow window)
        {
            this.window = window;
            dataGrid = window.DataGrid;
            newColumnNameComboBox = window.NewColumnNameComboBox;
            newColumnName = window.NewColumnName;
            removeColumn = window.RemoveColumn;
        }

        public void ExecuteToolTipOpening(object param)
        {
            MouseEventArgs e = param as MouseEventArgs;
            DataGridCell cell = GetCell(dataGrid, e);
            if (cell != null && cell.IsEnabled && cell.IsHitTestVisible && cell.Content is TextBlock)
            {
                SchemeColumn column = MainWindow.CurrentTable.Scheme.Columns[cell.Column.DisplayIndex];
                if (column.IsForeignKey)
                {
                    TextBlock textBlock = GetTheToolTip(column, cell);
                    ToolTip tip = new ToolTip();
                    tip.Content = textBlock;
                    ToolTipService.SetToolTip(cell, tip);
                }
            }
        }

        public static TextBlock GetTheToolTip(SchemeColumn selectedColumn, DataGridCell cell)
        {
            TextBlock textBlock = new TextBlock();
            Table table = TableInitializer.Tables[TableInitializer.FromFileNameToTableListIndex[selectedColumn.PrimaryTableLink.FileName]];
            SchemeColumn primaryColumn = table.Scheme.Columns[table.PrimaryColumnIndex];
            TextBlock cellText = cell.Content as TextBlock;
            DataRow[] foundRows = table.DataTable.Select($"{primaryColumn.Name}='{TableInitializer.GetTheValue(cellText.Text, primaryColumn)}'");
            int rowIndex = table.DataTable.Rows.IndexOf(foundRows[0]);

            foreach (SchemeColumn column in table.Scheme.Columns)
            {
                textBlock.Inlines.Add(new System.Windows.Documents.Run($"{column.Name}: ") { FontWeight = FontWeights.Bold });
                const int dayAndMonthLenInDate = 2;
                const int yearLenInDate = 4;
                const int hmsLenInDate = 2;

                if (column.Type == "date")
                {
                    DateTime date = DateTime.Parse(table.DataTable.Rows[rowIndex][column.Name].ToString());
                    string stringDate = $"{date.Day.ToString().PadLeft(dayAndMonthLenInDate, '0')}." +
                                $"{date.Month.ToString().PadLeft(dayAndMonthLenInDate, '0')}." +
                                $"{date.Year.ToString().PadLeft(yearLenInDate, '0')}";
                    textBlock.Inlines.Add(stringDate + Environment.NewLine);
                }
                else if (column.Type == "dateTime")
                {
                    DateTime dateTime = DateTime.Parse(table.DataTable.Rows[rowIndex][column.Name].ToString());
                    string stringDateTime = $"{dateTime.Day.ToString().PadLeft(dayAndMonthLenInDate, '0')}." +
                                $"{dateTime.Month.ToString().PadLeft(dayAndMonthLenInDate, '0')}." +
                                $"{dateTime.Year.ToString().PadLeft(yearLenInDate, '0')}" +
                                $" {dateTime.Hour.ToString().PadLeft(hmsLenInDate, '0')}:" +
                                $"{dateTime.Minute.ToString().PadLeft(hmsLenInDate, '0')}:" +
                                $"{dateTime.Second.ToString().PadLeft(hmsLenInDate, '0')}";
                    textBlock.Inlines.Add(stringDateTime + Environment.NewLine);
                }
                else
                {
                    textBlock.Inlines.Add(table.DataTable.Rows[rowIndex][column.Name].ToString() + Environment.NewLine);
                }
            }
            DeleteLastEnvironmentNewline(textBlock);

            return textBlock;
        }

        private DataGridCell GetCell(DataGrid dataGrid, MouseEventArgs e)
        {
            HitTestResult hitTest = VisualTreeHelper.HitTest(dataGrid, e.GetPosition(dataGrid));
            if (hitTest == null) 
                return null;

            DataGridCell cell = hitTest.VisualHit.GetParentOfType<DataGridCell>();
            return cell;
        }

        private static void DeleteLastEnvironmentNewline(TextBlock textBlock)
        {
            if (textBlock.Inlines.Count > 0 && textBlock.Inlines.LastInline is System.Windows.Documents.Run)
            {
                System.Windows.Documents.Run run = textBlock.Inlines.LastInline as System.Windows.Documents.Run;
                if (run.Text.EndsWith(Environment.NewLine))
                {
                    int start = run.Text.LastIndexOf(Environment.NewLine, StringComparison.Ordinal);
                    if (start >= 0)
                    {
                        run.Text = run.Text.Substring(0, start);
                        if (run.Text.Length == 0)
                        {
                            textBlock.Inlines.Remove(run);
                        }
                    }
                }
            }
        }

        public void ExecuteColumnRenaming(object param)
        {
            if (MainWindow.CurrentTable != null)
            {
                string newColumnName = param.ToString();
                if (newColumnName.Length == 0)
                {
                    Error emptyColNameError = new Error((int)ErrorsDisplayer.ErrorIndices.EmptyColumnNameIndex);
                    MessageBox.Show(emptyColNameError.Message, emptyColNameError.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else if (MainWindow.CurrentTable.DataTable.Columns.Contains(newColumnName))
                {
                    Error sameColNameError = new Error((int)ErrorsDisplayer.ErrorIndices.SameColumnNameIndex);
                    MessageBox.Show(sameColNameError.Message, sameColNameError.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    UpdateLinksColumnName(newColumnName);
                    MainWindow.CurrentTable.DataTable.Columns[newColumnNameComboBox.SelectedIndex].ColumnName = newColumnName;
                    MainWindow.CurrentTable.Scheme.Columns[newColumnNameComboBox.SelectedIndex].Name = newColumnName;
                    ComboBoxItem selectedItem = (ComboBoxItem)newColumnNameComboBox.SelectedItem;
                    selectedItem.Content = newColumnName;
                    UpdateTheDataGrid();
                    MainWindow.IsTableChanged = true;
                }
            }
        }

        private void UpdateLinksColumnName(string newColumnName)
        {
            foreach (SchemeColumn column in MainWindow.CurrentTable.Scheme.Columns)
            {
                if (column.IsPrimaryKey)
                {
                    for (int i = 0; i < column.ForeignTables.Count; i++)
                    {
                        List<Link> foreignTablesLinksList = column.ForeignTablesLinks.ToList();
                        Link foreignTableLink = foreignTablesLinksList[i];
                        Table foreignTable = column.ForeignTables[i];
                        string foreignColumnName = foreignTableLink.ColumnName;
                        foreach (SchemeColumn foreignTableColumn in foreignTable.Scheme.Columns)
                            if (foreignTableColumn.PrimaryTable == MainWindow.CurrentTable)
                                foreignTable.Scheme.Columns[foreignTableColumn.ForeignColumnIndex].PrimaryTableLink.ColumnName = newColumnName;
                            else break;
                    }
                }
                else if (column.IsForeignKey)
                    column.PrimaryTableLink.ColumnName = newColumnName;
            }
        }

        public void ExecuteNewColumnAddition(object param)
        {
            if (MainWindow.CurrentTable != null)
            {
                NewSchemeColumnAdding addingNewSchemeColumn = new NewSchemeColumnAdding(window);
                addingNewSchemeColumn.ShowDialog();
            }
        }

        public void ExecuteRowRemoval(object param)
        {
            int selectedRowIndex = dataGrid.SelectedIndex;

            if (selectedRowIndex != -1)
            {
                DataRow selectedRow = MainWindow.CurrentTable.DataTable.Rows[selectedRowIndex];
                for (int i = 0; i < MainWindow.CurrentTable.Scheme.Columns.Count; i++)
                {
                    SchemeColumn column = MainWindow.CurrentTable.Scheme.Columns[i];
                    if (column.IsPrimaryKey)
                    {
                        if (MainWindow.CurrentTable.UsedPrimaryElemsList.Contains(selectedRow[i].ToString()))
                        {
                            Error emptyColNameError = new Error((int)ErrorsDisplayer.ErrorIndices.UsedPrimaryValueRemovalIndex,
                                                      MainWindow.CurrentTable.Name, column.Name, selectedRow[i]);
                            MessageBox.Show(emptyColNameError.Message, emptyColNameError.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                            return;
                        }
                    }
                    else if (column.IsForeignKey)
                    {
                        column.PrimaryTable.UsedPrimaryElemsList.Remove(selectedRow[i].ToString());
                    }
                }
                MainWindow.CurrentTable.DataTable.Rows.RemoveAt(selectedRowIndex);
                MainWindow.IsTableChanged = true;
            }
        }

        public void ExecuteColumnRemoval(object param)
        {
            int index = Convert.ToInt32(param);
            SchemeColumn column = MainWindow.CurrentTable.Scheme.Columns[index];
            if (column.IsForeignKey)
                RemoveForeignTableLink(column);
            RemoveColumn(index);
            UpdateTheDataGrid();
            MainWindow.IsTableChanged = true;
        }

        public static void RemoveColumn(int i)
        {
            newColumnNameComboBox.Items.RemoveAt(i);
            MainWindow.CurrentTable.Scheme.Columns.RemoveAt(i);
            MainWindow.CurrentTable.DataTable.Columns.RemoveAt(i);
            removeColumn.Items.Clear();

            for (int j = 0; j < MainWindow.CurrentTable.Scheme.Columns.Count; j++)
                if (!MainWindow.CurrentTable.Scheme.Columns[j].IsPrimaryKey)
                    AddNewRemoveMenuItem(j);
        }

        public static void RemoveForeignTableLink(SchemeColumn column)
        {
            SchemeColumn primaryKeyColumn = column.PrimaryTable.Scheme.Columns[column.PrimaryTable.PrimaryColumnIndex];
            primaryKeyColumn.ForeignTables.Remove(MainWindow.CurrentTable);
            List<Link> foreignTablesLinksList = primaryKeyColumn.ForeignTablesLinks.ToList();
            for (int i = 0; i < primaryKeyColumn.ForeignTablesLinks.Count; i++)
            {
                Link foreignTableLink = foreignTablesLinksList[i];
                if (foreignTableLink.FileName == MainWindow.CurrentTable.Name &&
                    foreignTableLink.ColumnName == column.Name)
                    primaryKeyColumn.ForeignTablesLinks.Remove(foreignTableLink);
                break;
            }
        }

        public void ExecuteDataSaving(object param)
        {
            if (MainWindow.CurrentTable != null && MainWindow.IsTableChanged)
            {
                DataSaving.SaveDataInFile(MainWindow.CurrentTable);
                MainWindow.IsTableChanged = false;
                MessageBox.Show("The data in this file has been successfully updated.");
            }
        }

        public void ExecuteDataGridRowDeselecting(object param)
        {
            while (dataGrid.SelectedItem != null)
            {
                DataGridRow dataGridRow = dataGrid.ItemContainerGenerator.ContainerFromItem(dataGrid.SelectedItem) as DataGridRow;
                if (dataGridRow != null)
                    dataGridRow.IsSelected = false;
            }
        }

        private string oldValue { get; set; }

        public void ExecuteCellEditBeginning(object param)
        {
            DataGridBeginningEditEventArgs e = param as DataGridBeginningEditEventArgs;
            FrameworkElement cell = e.Column.GetCellContent(e.Row);
            TextBlock textBlock = cell as TextBlock;
            if (textBlock != null)
            {
                oldValue = textBlock.Text;
            }
        }

        public void ExecuteCellEditEnding(object param)
        {
            DataGridCellEditEndingEventArgs e = param as DataGridCellEditEndingEventArgs;
            FrameworkElement editedCell = e.Column.GetCellContent(e.Row);
            string newValue = ((TextBox)editedCell).Text;
            int index = e.Column.DisplayIndex;
            Error? primaryKeyColValueExistenceError = null;
            SchemeColumn column = MainWindow.CurrentTable.Scheme.Columns[index];
            if (column.IsForeignKey)
            {
                Table primaryTable = TableInitializer.Tables[TableInitializer.FromFileNameToTableListIndex[column.PrimaryTableLink.FileName]];
                primaryKeyColValueExistenceError = DoesValueExist(primaryTable, newValue);
            }

            if (primaryKeyColValueExistenceError != null)
            {
                e.Cancel = true;
                ((TextBox)editedCell).Text = oldValue;
                MessageBox.Show(primaryKeyColValueExistenceError.Message, primaryKeyColValueExistenceError.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                MainWindow.IsTableChanged = true;
            }
        }

        private Error? DoesValueExist(Table primaryTable, object newValue)
        {
            if (!primaryTable.PrimaryElemsList.Contains(newValue))
                return new Error((int)ErrorsDisplayer.ErrorIndices.PrimaryKeyColValueExistenceIndex, primaryTable.Name,
                                 primaryTable.TableFileInfo.Name, primaryTable.Scheme.Columns[primaryTable.PrimaryColumnIndex].Name, newValue);
            return null;
        }

        public static void DisplayCsvInDataGrid(TreeViewItem item)
        {
            FileInfo file = item.Tag as FileInfo;
            Error? currentTableInitializeError = null;
            Table selectedTable = TableInitializer.Tables[TableInitializer.FromFileNameToTableListIndex[file.Name]];
            if (!selectedTable.IsInitialized)
                currentTableInitializeError = TableInitializer.TryToInitializeTheTable(selectedTable);

            if (currentTableInitializeError == null) 
            {
                MainWindow.IsTableChanged = false;
                removeColumn.Items.Clear();
                newColumnNameComboBox.Items.Clear();
                dataGrid.ItemsSource = null;
                dataGrid.Columns.Clear();

                MainWindow.CurrentTable = selectedTable;
                Binding binding = new Binding();
                binding.Source = MainWindow.CurrentTable.DataTable.DefaultView;
                binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                dataGrid.SetBinding(ItemsControl.ItemsSourceProperty, binding);
                for (int i = 0; i < MainWindow.CurrentTable.Scheme.Columns.Count; i++) 
                {
                    BindDateTimeColumn(i);
                    AddColumnNameToLists(i);
                }

                if (newColumnNameComboBox.Items.Count != 0)
                    newColumnNameComboBox.SelectedItem = newColumnNameComboBox.Items[0];
                newColumnName.Text = "New column name";
            }
            else
            {
                MessageBox.Show(currentTableInitializeError.Message, currentTableInitializeError.Caption, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public static void BindDateTimeColumn(int i)
        {
            if (MainWindow.CurrentTable.Scheme.Columns[i].Type == "dateTime")
            {
                DataColumn column = MainWindow.CurrentTable.DataTable.Columns[i];
                column.DateTimeMode = DataSetDateTime.Unspecified;

                Binding binding = new Binding("[" + column.ColumnName + "]");
                binding.TargetNullValue = "";
                binding.Path = new PropertyPath(column.ColumnName);
                binding.Converter = new DateTimeConverter();

                DataGridTextColumn textColumn = new DataGridTextColumn();
                textColumn.Header = column.ColumnName;
                textColumn.Binding = binding;

                dataGrid.Columns.RemoveAt(i);
                dataGrid.Columns.Insert(i, textColumn);
            }
            else if (MainWindow.CurrentTable.Scheme.Columns[i].Type == "date")
            {
                DataColumn column = MainWindow.CurrentTable.DataTable.Columns[i];
                column.DateTimeMode = DataSetDateTime.Unspecified;

                Binding binding = new Binding("[" + column.ColumnName + "]");
                binding.TargetNullValue = "";
                binding.Path = new PropertyPath(column.ColumnName);
                binding.Converter = new DateConverter();

                DataGridTextColumn textColumn = new DataGridTextColumn();
                textColumn.Header = column.ColumnName;
                textColumn.Binding = binding;

                dataGrid.Columns.RemoveAt(i);
                dataGrid.Columns.Insert(i, textColumn);
            }
        }

        public static void AddColumnNameToLists(int i)
        {
            if (!MainWindow.CurrentTable.Scheme.Columns[i].IsPrimaryKey || MainWindow.CurrentTable.Scheme.Columns[i].ForeignTablesLinks.Count == 0)
                AddNewRemoveMenuItem(i);
            ComboBoxItem newItem = new ComboBoxItem();
            newItem.Content = MainWindow.CurrentTable.Scheme.Columns[i].Name;
            newColumnNameComboBox.Items.Add(newItem);
        }

        public static void AddNewRemoveMenuItem(int i)
        {
            MenuItem removeMenuItem = new MenuItem();
            removeMenuItem.Header = MainWindow.CurrentTable.Scheme.Columns[i].Name;
            removeMenuItem.Command = MainViewModel.RemoveColumn;
            removeMenuItem.CommandParameter = i;
            removeColumn.Items.Add(removeMenuItem);
        }

        public static void UpdateTheDataGrid()
        {
            dataGrid.ItemsSource = null;
            dataGrid.Columns.Clear();
            Binding binding = new Binding();
            binding.Source = MainWindow.CurrentTable.DataTable.DefaultView;
            binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            dataGrid.SetBinding(ItemsControl.ItemsSourceProperty, binding);
            for (int i = 0; i < MainWindow.CurrentTable.Scheme.Columns.Count; i++)
                BindDateTimeColumn(i);
        }
    }
}
